using UnityEngine;

namespace Pandora.Utils
{
    public static class Utilities
    {
        public static readonly float ARENA_MAX_SIZE = 14.5f;
        public static readonly float REWARD_FOR_PERFECT_DODGE = 5.0f;
        
        public static int ComputeReward(float rewardTime)
        {
            return Mathf.FloorToInt(REWARD_FOR_PERFECT_DODGE * (1 / Mathf.Pow(Mathf.Exp(rewardTime), 3)));
        }
    }
}