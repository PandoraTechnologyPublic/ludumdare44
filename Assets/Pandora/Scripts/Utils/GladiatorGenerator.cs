﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pandora.Utils
{
    public class GladiatorGenerator : MonoBehaviour
    {
        public TextAsset _nameList;
        private const string LINE_SEPARATOR = "\r\n"; // It defines line seperate character
        private const char FIELD_SEPARATOR = ';'; // It defines field seperate chracter
        private const bool SKIP_HEADER = true;
        private Dictionary<string, GladiatorBaseCharacter> _baseCharacters;

        // Start is called before the first frame update
        void Start()
        {
            LoadData();
        }

        // Update is called once per frame
        void Update()
        {

        }

        void LoadData()
        {
            string[] records = _nameList.text.Split(LINE_SEPARATOR.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            _baseCharacters = new Dictionary<string, GladiatorBaseCharacter>(records.Length);
            int index = -1;
            foreach (string record in records)
            {
                index++;
                if (SKIP_HEADER && index == 0) continue;

                string[] fields = record.Split(FIELD_SEPARATOR);

                GladiatorBaseCharacter baseCharacter = new GladiatorBaseCharacter();
                // Extract name
                //
                baseCharacter._name = fields[0].Trim();
                if (baseCharacter._name.Length == 0)
                {
                    Debug.LogError("Error while loading Text Asset: " + _nameList.name + " at line: " + index + " invalid name: " + fields[0]);
                    continue;                    
                }

                // Extract Gender
                //
                if (fields[1].Trim() == "M")
                {
                    baseCharacter._gender = Gender.MALE;
                }
                else if (fields[1].Trim() == "F")
                {
                    baseCharacter._gender = Gender.FEMALE;
                }
                else
                {
                    Debug.LogError("Error while loading Text Asset: " + _nameList.name + " at line: " + index + " invalid gender: " + fields[1]);
                    continue;
                }

                // Derivate main characteristic
                //
                string mainCharacteristics = fields[2].Trim();
                switch (mainCharacteristics)
                {
                    case "arrogance":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.SPEED;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.HEALTH;
                        break;
                    case "cruelty":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.SPEED;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.STRENGTH;
                        break;
                    case "destructiveness":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.STRENGTH;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.SPEED;
                        break;
                    case "ferocity":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.STRENGTH;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.HEALTH;
                        break;
                    case "martial":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.SPEED;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.STRENGTH;
                        break;
                    case "rashness":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.SPEED;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.HEALTH;
                        break;
                    case "speed":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.SPEED;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.HEALTH;
                        break;
                    case "strength":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.STRENGTH;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.HEALTH;
                        break;
                    case "virility":
                        baseCharacter._primaryCharacteristic = GladiatorCharacteristic.HEALTH;
                        baseCharacter._secondaryCharacteristic = GladiatorCharacteristic.STRENGTH;
                        break;
                    default:
                        Debug.LogError("Error while loading Text Asset: " + _nameList.name + " at line: " + index + " invalid mainCharacteristic: " + fields[2]);
                        continue;

                }

                _baseCharacters.Add(baseCharacter._name, baseCharacter);
            }
        }

        public GladiatorCharacter Generate(int level, string[] excludedNames)
        {
            // Filter the names to exclude
            //
            Dictionary<string, GladiatorBaseCharacter> inclusiveBaseCharacters = new Dictionary<string, GladiatorBaseCharacter>(_baseCharacters);
            foreach (string excludedName in excludedNames)
            {
                inclusiveBaseCharacters.Remove(excludedName);
            }

            // Pick-up a random base character
            //
            GladiatorBaseCharacter chosenGladiator = RandomValue(inclusiveBaseCharacters); 
            // Initiate based on its Base Character
            //
            GladiatorCharacter buffedChosenGladiator = new GladiatorCharacter();
            buffedChosenGladiator._name = chosenGladiator._name;
            buffedChosenGladiator._gender = chosenGladiator._gender;
            buffedChosenGladiator._primaryCharacteristic = chosenGladiator._primaryCharacteristic;
            buffedChosenGladiator._secondaryCharacteristic = chosenGladiator._secondaryCharacteristic;

            // Level up based on its level + main characteristics
            //
            buffedChosenGladiator._level = level;

            // FIXME make something smarter here !
            buffedChosenGladiator._health = level;
            buffedChosenGladiator._strength = level;
            buffedChosenGladiator._speed = level;

            return buffedChosenGladiator;
        }

        private TValue RandomValue<TKey, TValue>(IDictionary<TKey, TValue> dict)
        {
            return System.Linq.Enumerable.ToList(dict.Values)[UnityEngine.Random.Range(0, dict.Values.Count - 1)];
        }

        public enum Gender
        {
            MALE, FEMALE
        }

        public enum GladiatorCharacteristic
        {
            HEALTH, STRENGTH, SPEED
        }

        public class GladiatorBaseCharacter
        {
            public string _name;
            public Gender _gender;
            public GladiatorCharacteristic _primaryCharacteristic;
            public GladiatorCharacteristic _secondaryCharacteristic;
        }

        public class GladiatorCharacter : GladiatorBaseCharacter
        {

            public int _level;
            public int _health;
            public int _strength;
            public int _speed;
        }
    }
}
