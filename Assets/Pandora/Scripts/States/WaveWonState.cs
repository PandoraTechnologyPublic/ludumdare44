using Pandora.UI;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    public class WaveWonState : State
    {
        private readonly float _uiDisplayDelay = 3.0f;
        private float _enterTime;
        
        public override void OnEnter(GameStateManager gameStateManager)
        {
            _enterTime = Time.time;
            World.Active.EntityManager.CreateEntity(typeof(CleanLevelEventComponent));
        }
        
        public override void OnUpdate(GameStateManager gameStateManager, float deltaTime)
        {
            if (Time.time - _enterTime > _uiDisplayDelay)
            {
                gameStateManager.GetUiLayout().GetGameWonCanvas().gameObject.SetActive(true);
                
                if (gameStateManager.GetUiButtonDown("NextWave"))
                {
                    gameStateManager.NextWave();
                    gameStateManager.ChangeState<EnterGameState>();
                }
            }
        }

        public override void OnExit(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetGameWonCanvas().gameObject.SetActive(false);
        }
    }
}