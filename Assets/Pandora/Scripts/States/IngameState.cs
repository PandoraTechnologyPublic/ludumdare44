using UnityEngine;

namespace Pandora
{
    public class IngameState : State
    {
        private float elapsedTime;
        
        public override void OnEnter(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetInGameCanvas().gameObject.SetActive(true);
            elapsedTime = 0;
        }
        
        public override void OnUpdate(GameStateManager gameStateManager, float deltaTime)
        {
            elapsedTime += deltaTime;
            gameStateManager.GetUiLayout().SetGameTime(elapsedTime);
        }

        public override void OnExit(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetInGameCanvas().gameObject.SetActive(false);
        }
    }
}