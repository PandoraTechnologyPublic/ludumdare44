﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora
{
    public class UiLayout : MonoBehaviour
    {
        [SerializeField] private Canvas _welcomeCanvas;
        [SerializeField] private Canvas _mainMenuCanvas;
        [SerializeField] private Canvas _inGameCanvas;
        [SerializeField] private Canvas _gameOverCanvas;
        [SerializeField] private Canvas _gameWonCanvas;
        [SerializeField] private Canvas _creditsCanvas;
        [SerializeField] private RawImage _gameOverThumbImage;
        [SerializeField] private Button[] _rawButtons;
        [SerializeField] private Image _heroAvatarImage;
        [SerializeField] private TMP_Text _heroNameText;
        [SerializeField] private Slider _heroHealthSlider;
        [SerializeField] private TMP_Text _heroHealthText;
        [SerializeField] private Slider _heroStaminaSlider;
        [SerializeField] private TMP_Text _heroRewardText;
        [SerializeField] private TMP_Text _gameTimeText;
        [SerializeField] private TMP_Text _gameInfoText;
        private Dictionary<string, Button> _buttons;


        private const string BUTTON_SUFFIX = "Button";
        private const int HERO_HEALTH_WIDTH = 2;

        void Awake()
        {
            // Disable all Canvas
            //
            _welcomeCanvas.gameObject.SetActive(false);
            _mainMenuCanvas.gameObject.SetActive(false);
            _inGameCanvas.gameObject.SetActive(false);
            _gameOverCanvas.gameObject.SetActive(false);
            _creditsCanvas.gameObject.SetActive(false);

            // Register buttons
            //
            _buttons = new Dictionary<string, Button>();
            
            foreach (Button button in _rawButtons)
            {
                if (!button.name.Contains(BUTTON_SUFFIX))
                {
                    Debug.LogError("Button: " + button.name + " does NOT respect naming convention, i.e. suffix should be: " + BUTTON_SUFFIX);

                }
                else
                {
                    _buttons.Add(button.name.Remove(button.name.Length - BUTTON_SUFFIX.Length, BUTTON_SUFFIX.Length), button);
                }
            }
            
        }

        public void SetGameTime(float elapsedTime)
        {
            string humanReadableTime = "";
            int seconds = Mathf.FloorToInt((elapsedTime ) % 60);
            int minutes = Mathf.FloorToInt((elapsedTime / 60) % 60);
            int hours = Mathf.FloorToInt((elapsedTime / (60 *60)) % 60);
            if (hours > 0)
            {
                humanReadableTime += hours + "h";

                if (minutes < 10)
                {
                    humanReadableTime += 0;
                }
            }
            if (minutes > 0)
            {
                humanReadableTime += minutes + "m";

                if (seconds < 10)
                {
                    humanReadableTime += 0;
                }
            }

            humanReadableTime += seconds;

            _gameTimeText.text = humanReadableTime;
        }

        public void SetGameWaveInfo(int waveNumber, int aliveOpponents, int deadOpponents)
        {
            _gameInfoText.text = "Wave " + waveNumber + " (" + aliveOpponents + "/" + (aliveOpponents + deadOpponents) + ")";
        }

        public void SetHeroHealthStats(float currentHealth, float maxHealth)
        {
            _heroHealthSlider.value = (currentHealth / maxHealth) * 100;
            _heroHealthText.SetText($"{currentHealth}/{maxHealth}");
//            _heroHealthSlider.GetComponent<RectTransform>().sizeDelta = new Vector2(Mathf.RoundToInt(maxHealth) * HERO_HEALTH_WIDTH,
//                _heroHealthSlider.GetComponent<RectTransform>().sizeDelta.y);
        }

        public void SetHeroStaminaStats(float currentStamina, float maxStamina)
        {
            _heroStaminaSlider.value = (currentStamina / maxStamina) * 100;
        }

        public void SetHeroInfo(string heroName)
        {
            _heroNameText.text = heroName;
        }

        public void SetHeroReward(int earnMoney)
        {
            string humanReadableAmount = "";

            int tens = earnMoney % 1000;
            int tousands = (earnMoney / 1000) % 1000;
            int millions = (earnMoney / 1000000) % 1000;

            if (millions > 0)
            {
                humanReadableAmount += millions + "'";
            }
            if (tousands > 0)
            {
                humanReadableAmount += tousands + "'";
            }
            humanReadableAmount += tens;

            _heroRewardText.text = humanReadableAmount + " $";
        }

        public Canvas GetWelcomeCanvas()
        {
            return _welcomeCanvas;
        }

        public Canvas GetMainMenuCanvas()
        {
            return _mainMenuCanvas;
        }

        public Canvas GetInGameCanvas()
        {
            return _inGameCanvas;
        }

        public Canvas GetGameOverCanvas()
        {
            return _gameOverCanvas;
        }

        public Canvas GetGameWonCanvas()
        {
            return _gameWonCanvas;
        }

        public Canvas GetCreditsCanvas()
        {
            return _creditsCanvas;
        }

        public RawImage GetGameOverThumbImage()
        {
            return _gameOverThumbImage;
        }

        public Image GetHeroAvatarImage()
        {
            return _heroAvatarImage;
        }

        public Dictionary<string, Button> GetButtons()
        {
            return _buttons;
        }
    }
}
