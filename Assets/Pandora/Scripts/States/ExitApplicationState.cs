using UnityEditor;
using UnityEngine;

namespace Pandora
{
    public class ExitApplicationState : State
    {
        public override void OnEnter(GameStateManager gameStateManager)
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}