using Unity.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora
{
    public class GameOverState : State
    {
        private readonly float _uiDisplayDelay = 3.0f;
        private float _enterTime;
        private int _spectatorDecision = 0;

        public override void OnEnter(GameStateManager gameStateManager)
        {
            _enterTime = Time.time;
            _spectatorDecision = Random.Range(0, 10);
            
            if (_spectatorDecision > 5)
            {
                
                gameStateManager.GetUiLayout().GetGameOverThumbImage().transform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
            }
            else
            {
                gameStateManager.GetUiLayout().GetGameOverThumbImage().transform.rotation = Quaternion.AngleAxis(180, Vector3.forward);
            }
        }
        
        public override void OnUpdate(GameStateManager gameStateManager, float deltaTime)
        {
            if (Time.time - _enterTime > _uiDisplayDelay)
            {
                gameStateManager.GetUiLayout().GetGameOverCanvas().gameObject.SetActive(true);
                
                if (Input.anyKey)
                {
                    gameStateManager.ChangeState<MainMenuState>();
                }
            }
        }

        public override void OnExit(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetGameOverCanvas().gameObject.SetActive(false);
            
            foreach (Entity entity in World.Active.EntityManager.GetAllEntities())
            {
                World.Active.EntityManager.DestroyEntity(entity);
            }

            SceneManager.LoadScene(0);
        }
    }
}