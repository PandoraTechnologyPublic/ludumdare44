using UnityEngine;

namespace Pandora
{
    public class State : MonoBehaviour
    {
        public virtual void OnEnter(GameStateManager gameStateManager)
        {
            
        }

        public virtual void OnUpdate(GameStateManager gameStateManager, float deltaTime)
        {
            
        }

        public virtual void OnExit(GameStateManager gameStateManager)
        {
            
        }
    }
}