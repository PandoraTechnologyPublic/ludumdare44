using UnityEngine;

namespace Pandora
{
    public class CreditsState : State
    {
        public override void OnUpdate(GameStateManager gameStateManager, float deltaTime)
        {
            if (Input.anyKey)
            {
                gameStateManager.ChangeState<MainMenuState>();
            }
        }

        public override void OnEnter(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetCreditsCanvas().gameObject.SetActive(true);
        }

        public override void OnExit(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetCreditsCanvas().gameObject.SetActive(false);
        }
    }
}