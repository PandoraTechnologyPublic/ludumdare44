using UnityEngine;

namespace Pandora
{
    public class MainMenuState : State
    {
        public override void OnUpdate(GameStateManager gameStateManager, float deltaTime)
        {
            if (gameStateManager.GetUiButtonDown("StartGame"))
            {
                gameStateManager.ChangeState<EnterGameState>();
            }
            else if (gameStateManager.GetUiButtonDown("Credits"))
            {
                gameStateManager.ChangeState<CreditsState>();
            }
            else if (gameStateManager.GetUiButtonDown("Exit"))
            {
                gameStateManager.ChangeState<ExitApplicationState>();
            }
        }

        public override void OnEnter(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetMainMenuCanvas().gameObject.SetActive(true);
        }

        public override void OnExit(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetMainMenuCanvas().gameObject.SetActive(false);
        }
    }
}