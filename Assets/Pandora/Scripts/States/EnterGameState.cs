using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    public class EnterGameState : State
    {
        public override void OnEnter(GameStateManager gameStateManager)
        {
            EntityManager activeEntityManager = World.Active.EntityManager;
            Entity entity = activeEntityManager.CreateEntity();
            activeEntityManager.AddComponentData(entity, new GenerateLevelEventComponent());
        }

        public override void OnUpdate(GameStateManager gameStateManager, float deltaTime)
        {
            
        }
    }
}