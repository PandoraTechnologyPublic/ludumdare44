using Pandora.UI;
using Pandora.Utils;
using System;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using static Pandora.Utils.GladiatorGenerator;

namespace Pandora
{
    public class GameStateManager : MonoBehaviour
    {
        [SerializeField] private UiLayout _uiLayout;
        [SerializeField] private SoundManager _soundManager;
        [SerializeField] private bool skipMenu;
        [SerializeField] private GladiatorGenerator gladiatorGenerator;
        private Dictionary<Type, State> _states = new Dictionary<Type, State>();    
        private State _currentState;
        private State _previousState;
        private string _uiButtonDown;
        private int _currentWave = 1;
        private List<string> _gladiatorNames = new List<string>();

        private void Awake()
        {
            InitialiseState<WelcomeState>();
            InitialiseState<MainMenuState>();
            InitialiseState<EnterGameState>();
            InitialiseState<IngameState>();
            InitialiseState<ExitApplicationState>();
            InitialiseState<GameOverState>();
            InitialiseState<WaveWonState>();
            InitialiseState<CreditsState>();

            if (skipMenu)
            {
                _currentState = GetState<EnterGameState>();
            }
            else
            {
                _currentState = GetState<WelcomeState>();
            }

            _soundManager.PlayMainLoop();
            
            _previousState = _currentState;
            _currentState.OnEnter(this);


            foreach (KeyValuePair<string, Button> button in _uiLayout.GetButtons())
            {
                button.Value.onClick.AddListener(() => PressUiButton(button.Key));
            }
        }

        private void Update()
        {
            _currentState.OnUpdate(this, Time.deltaTime);
            _uiButtonDown = null;
        }

        private void InitialiseState<TYPE_STATE>() where TYPE_STATE : State
        {
            Type locStateType = typeof(TYPE_STATE);
            TYPE_STATE locState = new GameObject(locStateType.ToString()).AddComponent<TYPE_STATE>();

            _states.Add(locStateType, locState);
            locState.transform.SetParent(transform, false);
        }

        private TYPE_STATE GetState<TYPE_STATE>() where TYPE_STATE : State
        {
            Type locStateType = typeof(TYPE_STATE);

            return (TYPE_STATE) _states[locStateType];
        }

        public void ChangeState<TYPE_STATE>() where TYPE_STATE : State
        {
            Type locStateType = typeof(TYPE_STATE);
            
            Debug.Log($"Change state {locStateType}");

            Assert.IsTrue(_states.ContainsKey(locStateType));
            Assert.AreNotEqual(_currentState.GetType(), locStateType);

            _currentState.OnExit(this);

            _previousState = _currentState;
            _currentState = GetState<TYPE_STATE>();
            _currentState.OnEnter(this);
        }

        public void PressUiButton(string buttonName)
        {
            _uiButtonDown = buttonName;
        }

        public bool GetUiButtonDown(string buttonName)
        {
            return (buttonName == _uiButtonDown);
        }

        public bool IsCurrentState<TYPE_STATE>() where TYPE_STATE : State
        {
            Type locStateType = typeof(TYPE_STATE);

            return (_currentState != null) && (_currentState.GetType() == locStateType);
        }

        public TYPE_STATE GetCurrentState<TYPE_STATE>() where TYPE_STATE : State
        {
            return _currentState as TYPE_STATE;
        }

        public UiLayout GetUiLayout()
        {
            return _uiLayout;
        }

        public void WaveWon(Entity heroEntity, int currentHealth, int currentMaxHealth, int currentMaxStamina, int currentAttack, int pointsToDistribute)
        {
            if (!IsCurrentState<WaveWonState>())
            {
                GameWonScreenController controller = _uiLayout.GetGameWonCanvas().gameObject.GetComponent<GameWonScreenController>();
                controller.Initialize(currentHealth, currentMaxHealth, currentMaxStamina, currentAttack, pointsToDistribute, (order) => ApplyImprovements(heroEntity, order));
                ChangeState<WaveWonState>();
            }
        }

        public void ApplyImprovements(Entity heroEntity, HeroImprovementsOrder order)
        {
            EntityManager activeEntityManager = World.Active.EntityManager;

            LifeComponent lifeComponent = activeEntityManager.GetComponentData<LifeComponent>(heroEntity);
            lifeComponent._maxLife += order._increasedHealth;
            
            lifeComponent._currentLife = order._newCurrentHealth;
            
            activeEntityManager.SetComponentData(heroEntity, lifeComponent);


            StaminaComponent staminaComponent = activeEntityManager.GetComponentData<StaminaComponent>(heroEntity);
            staminaComponent._maxStamina += order._increasedStamina;
            activeEntityManager.SetComponentData(heroEntity, staminaComponent);

            RewardComponent rewardComponent = activeEntityManager.GetComponentData<RewardComponent>(heroEntity);
            rewardComponent._currentReward -= order._pointsDistributed;
            activeEntityManager.SetComponentData(heroEntity, rewardComponent);

            AttackComponent attackComponent = activeEntityManager.GetComponentData<AttackComponent>(heroEntity);
            attackComponent._damage += order._increasedAttack;
            activeEntityManager.SetComponentData(heroEntity, attackComponent);
            
//            Debug.Log("Apply following improvement: "
//                + "health: " + order._increasedHealth
//                + " | "
//                + "stamina: " + order._increasedStamina);
        }

        public void WaveLoose()
        {
            if (!IsCurrentState<GameOverState>())
            {
                ChangeState<GameOverState>();
            }                
        }

        public void NextWave()
        {
            _currentWave++;
            _gladiatorNames.Clear();
        }

        public int GetCurrentWave()
        {
            return _currentWave;
        }

        public string GenerateEnemy()
        {
            GladiatorCharacter enemyCharacter = gladiatorGenerator.Generate(_currentWave, _gladiatorNames.ToArray());
            _gladiatorNames.Add(enemyCharacter._name);
            return enemyCharacter._name;
        }
    }
}