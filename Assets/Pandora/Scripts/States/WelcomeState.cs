using UnityEngine;

namespace Pandora
{
    public class WelcomeState : State
    {
        public override void OnUpdate(GameStateManager gameStateManager, float deltaTime)
        {
            if (Input.anyKey)
            {
                gameStateManager.ChangeState<MainMenuState>();
            }
        }

        public override void OnEnter(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetWelcomeCanvas().gameObject.SetActive(true);
        }

        public override void OnExit(GameStateManager gameStateManager)
        {
            gameStateManager.GetUiLayout().GetWelcomeCanvas().gameObject.SetActive(false);
        }
    }
}