using Unity.Entities;

namespace Pandora
{
    public struct DodgeEventComponent : IComponentData
    {
        public Entity _dodgingEntity;
        public float _dodgeDuration;
    }
}