using Unity.Entities;

namespace Pandora
{
    public struct InputComponent : IComponentData
    {
        public float _horizontalAxis;
        public float _verticalAxis;
        public float _rotationAxis;
        public int _attackButtonStatus;
        public int _dodgeButtonStatus;
    }
}