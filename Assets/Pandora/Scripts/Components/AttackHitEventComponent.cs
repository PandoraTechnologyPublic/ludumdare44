using Unity.Entities;

namespace Pandora
{
    public struct AttackHitEventComponent : IComponentData
    {
        public Entity _hitDealerEntity;
        public Entity _hitReceiverEntity;
        public int _damage;
    }
}