using Unity.Entities;

namespace Pandora
{
    public struct RotationComponent : IComponentData
    {
        public float _rotationSpeed;
    }
}