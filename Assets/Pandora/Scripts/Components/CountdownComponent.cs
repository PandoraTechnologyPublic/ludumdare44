using Unity.Entities;

namespace Pandora
{
    public struct CountdownComponent : IComponentData
    {
        public float _timeRemaining;
    }
}