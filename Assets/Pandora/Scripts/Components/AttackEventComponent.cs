using Unity.Collections;
using Unity.Entities;

namespace Pandora
{
    public struct AttackEventComponent : IComponentData
    {
        public Entity _attackingEntity;
        public Entity _lastDodgerEntity;
        public float _attackDuration;
//        public float _maxOverlapDistance;
        public float _rewardTime;
        public int _hitCount;
    }
}