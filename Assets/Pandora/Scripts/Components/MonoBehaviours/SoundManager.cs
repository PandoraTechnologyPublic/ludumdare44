﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioSource _musicSource;
    [SerializeField] private AudioSource _sfxSource;

    [Header("Music")]
    [SerializeField] private AudioClip _mainLoop;

    [Header("SFX")]
    [SerializeField] private AudioClip[] _footsteps;
    [SerializeField] private AudioClip[] _dodges;
    [SerializeField] private AudioClip[] _strikes;
    [SerializeField] private AudioClip[] _hits;
    [SerializeField] private AudioClip[] _enemyDeaths;
    [SerializeField] private AudioClip _playerDeath;
    [SerializeField] private AudioClip _notEnoughStamina;

    public void PlayMainLoop()
    {
        _musicSource.loop = true;
        _musicSource.clip = _mainLoop;
        _musicSource.Play();
    }

    public void PlayRandomSound(AudioClip[] from)
    {
        if (from.Length > 0)
        {
            _sfxSource.PlayOneShot(from[Random.Range(0, from.Length)]);
        }
    }

    public void PlayStep()
    {
        PlayRandomSound(_footsteps);
    }

    public void PlayDodge()
    {
        PlayRandomSound(_dodges);
    }

    public void PlayStrike()
    {
        PlayRandomSound(_strikes);
    }

    public void PlayHit()
    {
        PlayRandomSound(_hits);
    }

    public void PlayEnemyDeath()
    {
        PlayRandomSound(_enemyDeaths);
    }

    public void PlayPlayerDeath()
    {
        _sfxSource.PlayOneShot(_playerDeath);
    }

    public void PlayNotEnoughStamina()
    {
        _sfxSource.PlayOneShot(_notEnoughStamina);
    }
}
