using System.Collections;
using TMPro;
using UnityEngine;

namespace Pandora.Components.MonoBehaviours
{
    public class LifeDecreaseLayout : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _lifeDecreaseTransform;
        [SerializeField] private TMP_Text _lifeDecreaseText;
        [SerializeField] private float _lifeDecreaseAnimationDuration;
        [SerializeField] private float _rewardAnimLength;
        
        private Vector3 _initialTransformLocalPosition;
        private Coroutine _animationCoroutine;

        private void Awake()
        {
            _lifeDecreaseTransform.gameObject.SetActive(false);
            _initialTransformLocalPosition = _lifeDecreaseTransform.localPosition;
        }

        public void StartLifeDecreaseAnimation()
        {
            if (_animationCoroutine != null)
            {
                StopCoroutine(_animationCoroutine);
            }
            
            _animationCoroutine = StartCoroutine(AnimationCoroutine());
            _animator.SetTrigger("shot");
        }

        private IEnumerator AnimationCoroutine()
        {
            ResetLayout();
            _lifeDecreaseTransform.gameObject.SetActive(true);

            yield return new WaitForSeconds(_lifeDecreaseAnimationDuration);
            ResetLayout();
            _animationCoroutine = null;
        }

        public void ResetLayout()
        {
            _lifeDecreaseTransform.gameObject.SetActive(false);
            _lifeDecreaseTransform.localPosition = _initialTransformLocalPosition;
        }

        public void Initialize(int liveDecreaseValue)
        {
            _lifeDecreaseText.text = "-" + liveDecreaseValue;
        }
    }
}