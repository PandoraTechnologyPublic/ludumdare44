﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    private SoundManager _soundManager;

    private void FindSoundManager()
    {
        GameObject soundManagerObj = GameObject.FindGameObjectWithTag("sound_manager");
        _soundManager = soundManagerObj.GetComponent<SoundManager>();
    }

    private void Awake()
    {
        FindSoundManager();
    }

    public void PlaySound(SoundBank bank)
    {
        if (_soundManager != null)
        {
            _soundManager.PlayRandomSound(bank.sounds);
        }
        else
        {
            FindSoundManager(); // let's try to have it next time...
        }
    }
}
