using System.Collections;
using TMPro;
using UnityEngine;

namespace Pandora.Components.MonoBehaviours
{
    public class EnemyNameLayout : MonoBehaviour
    {
        [SerializeField] private TMP_Text _enemnyNameText;

        public void SetNAme(string name)
        {
            _enemnyNameText.text = name;
        }

        public void DisableLayout()
        {
            _enemnyNameText.enabled = false;
        }
    }
}