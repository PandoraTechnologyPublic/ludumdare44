using System.Collections;
using UnityEngine;

namespace Pandora
{
    public class AttackBehaviourComponent : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _damageZone;
//        [SerializeField] private Collider _attackHitCollider;
//        [SerializeField] private Collider _attackMissCollider;
        [SerializeField] private float _timeBeforeDamageActivation;
        [SerializeField] private float _timeBeforeDamageDeactivation;
        
        private Coroutine _attackCoroutine;
        private bool _isDealingDamage;
        private bool _isGivingReward;
        private bool _hasDealtDamage;
        private float _coroutineStart;
        private float _timeToDamageDealing;

        private void Awake()
        {
            _isDealingDamage = false;
            _isGivingReward = false;
            _hasDealtDamage = false;

            if (_damageZone != null)
            {
                _damageZone.gameObject.SetActive(false);
            }
        }        
        
        public bool IsDealingDamage()
        {
            return _isDealingDamage && !_hasDealtDamage;
        }

        public float GetTimeToDamageDealing()
        {
            return Mathf.Max(0, _timeBeforeDamageActivation - (Time.time - _coroutineStart));
        }

        public bool IsGivingReward()
        {
            return _isGivingReward;
        }

        public void StartAttackAnimation()
        {
            if (_attackCoroutine == null)
            {
                _attackCoroutine = StartCoroutine(AttackCoroutine());
            }
        }

        private IEnumerator AttackCoroutine()
        {
            _animator.SetTrigger("attacking");
            _coroutineStart = Time.time;
            _isDealingDamage = false;
            _isGivingReward = true;
//            Debug.Log("GiveReward");

            if (_damageZone != null)
            {
                _damageZone.gameObject.SetActive(true);
            }
            
            yield return  new WaitForSeconds(_timeBeforeDamageActivation);
            
//            Debug.Break();

//            Debug.Log("GiveDamage");
            _isDealingDamage = true;
            _isGivingReward = false;
            
            yield return  new WaitForSeconds(_timeBeforeDamageDeactivation);
            
            if (_damageZone != null)
            {
                _damageZone.gameObject.SetActive(false);
            }
            
//            Debug.Break();

//            Debug.Log("End");
            _isDealingDamage = false;
            _isGivingReward = false;
            _hasDealtDamage = false;
            
            
            _attackCoroutine = null;
        }

        public void SetHasDealtDamage(bool hasDealtDamage)
        {
            _hasDealtDamage = hasDealtDamage;
        }
    }
}