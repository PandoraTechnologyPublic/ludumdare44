using System.Collections;
using TMPro;
using UnityEngine;

namespace Pandora.Components.MonoBehaviours
{
    public class RewardBehaviourComponent : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private TMP_Text _rewardValueText;
        [SerializeField] private float _rewardAnimLength;

        private Coroutine _hideTextCoroutine;
        private Color _initialColor;

        private void Awake()
        {
            _rewardValueText.gameObject.SetActive(false);
            _initialColor = _rewardValueText.color;
        }

        public void SetRewardValue(float rewardValue)
        {
            _rewardValueText.color = _initialColor;
            _rewardValueText.gameObject.SetActive(true);
            _rewardValueText.SetText($"{rewardValue:F2}$");
        }

        public void ShowRewardLostEffect()
        {
            _rewardValueText.color = Color.red;
            
            if (_hideTextCoroutine != null)
            {
                StopCoroutine(_hideTextCoroutine);
            }
            
            _hideTextCoroutine = StartCoroutine(HideTextDelay(0.5f));

            _animator.SetTrigger("shot");
        }       
        
        public void ShowRewardWonEffect()
        {
            _rewardValueText.color = Color.green;
            
            if (_hideTextCoroutine != null)
            {
                StopCoroutine(_hideTextCoroutine);
            }
            _hideTextCoroutine = StartCoroutine(HideTextDelay(0.5f));

            _animator.SetTrigger("shot");
        }

        private IEnumerator HideTextDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            _rewardValueText.gameObject.SetActive(false);
        }
    }
}