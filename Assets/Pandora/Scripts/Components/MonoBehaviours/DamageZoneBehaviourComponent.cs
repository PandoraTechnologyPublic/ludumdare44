using Pandora.Components.MonoBehaviours;
using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    public class DamageZoneBehaviourComponent : MonoBehaviour
    {
        private bool hasDealtDamage;
        
        private void OnTriggerEnter(Collider otherCollider)
        {
            hasDealtDamage = false;
        }

        private void OnTriggerStay(Collider otherCollider)
        {
            AttackBehaviourComponent attackBehaviourComponent = otherCollider.GetComponentInParent<AttackBehaviourComponent>();
            
            if (attackBehaviourComponent.IsDealingDamage() && !hasDealtDamage)
            {
                EntityComponent receiverEntityComponent = GetComponentInParent<EntityComponent>();

                if (receiverEntityComponent != null)
                {
//                Debug.Log("Collide with " +  other.transform.name);
                    EntityManager activeEntityManager = World.Active.EntityManager;
                    EntityComponent dealerEntityComponent = otherCollider.GetComponentInParent<EntityComponent>();

                    if (dealerEntityComponent != receiverEntityComponent)
                    {
                        Entity entity = activeEntityManager.CreateEntity(typeof(AttackHitEventComponent));
                        AttackComponent attack = activeEntityManager.GetComponentData<AttackComponent>(dealerEntityComponent.GetEntity());
                        activeEntityManager.SetComponentData(entity, new AttackHitEventComponent {_damage = attack._damage, _hitDealerEntity = dealerEntityComponent.GetEntity(), _hitReceiverEntity = receiverEntityComponent.GetEntity()});

                        attackBehaviourComponent.SetHasDealtDamage(true);
                    }
                }
            }
            else if (attackBehaviourComponent.IsGivingReward())
            {
                float rewardTiming = attackBehaviourComponent.GetTimeToDamageDealing();
                
                EntityComponent receiverEntityComponent = GetComponentInParent<EntityComponent>();
                EntityComponent dealerEntityComponent = otherCollider.GetComponentInParent<EntityComponent>();

                if (receiverEntityComponent != null && dealerEntityComponent != null && receiverEntityComponent != dealerEntityComponent)
                {
                    EntityManager activeEntityManager = World.Active.EntityManager;

//                    if (collision.contactCount > 0)
//                    {
                        Entity receiverEntity = receiverEntityComponent.GetEntity();
                        Entity dealerEntity = dealerEntityComponent.GetEntity();
                        Entity attackMissEntity = activeEntityManager.CreateEntity();
                        AttackMissEventComponent attackMissEvent = new AttackMissEventComponent {_overlapDistance = 0, _attackerEntity = dealerEntity, _defenderEntity = receiverEntity};

//                        foreach (ContactPoint contactPoint in collision.contacts)
//                        {
//                            if (contactPoint.separation <= 0) // means the two colliders are overlapping
//                            {
//                                float overlapDist = -contactPoint.separation;
//
//                                if (overlapDist > attackMissEvent._overlapDistance) // the greatest overlapDist is, the more overlapping they are -> very near the weapon!
//                                {
                                    attackMissEvent._rewardTime = rewardTiming;
//                                    attackMissEvent._overlapDistance = rewardTiming;
                                    attackMissEvent._defenderEntity = receiverEntity;
//                                }
//                            }
//                        }

                        activeEntityManager.AddComponentData(attackMissEntity, attackMissEvent);
//                    }
                }
            }
//            EntityComponent receiverEntityComponent = other.gameObject.GetComponentInParent<EntityComponent>();
//            EntityComponent dealerEntityComponent = GetComponentInParent<EntityComponent>();
//
//            if (receiverEntityComponent != null && dealerEntityComponent != null && receiverEntityComponent != dealerEntityComponent)
//            {
//                EntityManager activeEntityManager = World.Active.EntityManager;
//
//                if (collision.contactCount > 0)
//                {
//                    Entity receiverEntity = receiverEntityComponent.GetEntity();
//                    Entity dealerEntity = dealerEntityComponent.GetEntity();
//                    Entity attackMissEntity = activeEntityManager.CreateEntity();
//                    AttackMissEventComponent attackMissEvent = new AttackMissEventComponent {_overlapDistance = 0, _attackerEntity = dealerEntity, _defenderEntity = receiverEntity};
//
//                    foreach (ContactPoint contactPoint in collision.contacts)
//                    {
//                        if (contactPoint.separation <= 0) // means the two colliders are overlapping
//                        {
//                            float overlapDist = -contactPoint.separation;
//
//                            if (overlapDist > attackMissEvent._overlapDistance) // the greatest overlapDist is, the more overlapping they are -> very near the weapon!
//                            {
//                                attackMissEvent._overlapDistance = overlapDist;
//                                attackMissEvent._defenderEntity = receiverEntity;
//                            }
//                        }
//                    }
//
//                    activeEntityManager.AddComponentData(attackMissEntity, attackMissEvent);
//                }
//            }
        }
        
//        private void OnCollisionStay(Collision collision)
//        {
//            EntityComponent receiverEntityComponent = collision.gameObject.GetComponentInParent<EntityComponent>();
//            EntityComponent dealerEntityComponent = GetComponentInParent<EntityComponent>();
//            
//            if (receiverEntityComponent != null && dealerEntityComponent != null && receiverEntityComponent != dealerEntityComponent)
//            {
//                EntityManager activeEntityManager = World.Active.EntityManager;
//                
//                if (collision.contactCount > 0)
//                {
//                    Entity receiverEntity = receiverEntityComponent.GetEntity();
//                    Entity dealerEntity = dealerEntityComponent.GetEntity();
//                    Entity attackMissEntity = activeEntityManager.CreateEntity();
//                    AttackMissEventComponent attackMissEvent = new AttackMissEventComponent { _overlapDistance = 0, _attackerEntity = dealerEntity, _defenderEntity = receiverEntity};
//                    
//                    foreach (ContactPoint contactPoint in collision.contacts)
//                    {
//                        if (contactPoint.separation <= 0) // means the two colliders are overlapping
//                        {
//                            float overlapDist = -contactPoint.separation;
//                            
//                            if (overlapDist > attackMissEvent._overlapDistance) // the greatest overlapDist is, the more overlapping they are -> very near the weapon!
//                            {
//                                attackMissEvent._overlapDistance = overlapDist;
//                                attackMissEvent._defenderEntity = receiverEntity;
//                            }
//                        }
//                    }
//
//                    activeEntityManager.AddComponentData(attackMissEntity, attackMissEvent);
//                }
//            }
//        }
    }
}