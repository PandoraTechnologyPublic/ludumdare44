﻿using UnityEngine;

namespace Pandora.Components.MonoBehaviours
{
    [ExecuteInEditMode]
    public class LookAtCamera : MonoBehaviour
    {
        private void Update()
        {
            transform.LookAt(Camera.main.transform);
        }
    }
}
