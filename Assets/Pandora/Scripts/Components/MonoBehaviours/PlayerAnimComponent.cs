﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimComponent : MonoBehaviour
{
    [SerializeField] private float runAnimSpeed = 1.0f;
    [SerializeField] private bool cheatTestAnim = false;
    
    private Animator anim;
    private Vector3 previousPosition;

    // Start is called before the first frame update
    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
        previousPosition = transform.position;
    }

    // Update is called once per frame
    private void Update()
    {
        if(cheatTestAnim)
        {
            if (Input.GetButtonDown("Fire1") == true)
            {
                anim.SetTrigger("attacking");
            }
            if (Input.GetButtonDown("Dodge") == true)
            {
                anim.SetTrigger("dodge");
            }
        }
        
        Vector3 position = transform.position;
        float speed = (position - previousPosition).sqrMagnitude * runAnimSpeed;
        anim.SetFloat("speed", speed);

        previousPosition = position;
    }
}
