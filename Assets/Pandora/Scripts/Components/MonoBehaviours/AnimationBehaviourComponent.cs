using UnityEngine;
using UnityEngine.AI;

namespace Pandora.Components.MonoBehaviours
{
    public class AnimationBehaviourComponent : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        public void StartHitAnimation()
        {
            _animator.SetTrigger("hit");
        }
    
        public void StartDodgeAnimation()
        {
            _animator.SetTrigger("dodge");
        }
        
        public void StartDeathAnimation()
        {
            _animator.SetTrigger("death");
            
            NavMeshAgent navMeshAgent = GetComponent<NavMeshAgent>();
            
            if (navMeshAgent)
            {
                GetComponent<NavMeshAgent>().radius = 0.5f;
            }
            
//            GameObject.FindObjectOfType<SoundManager>().PlayPlayerDeath();
            
//            Debug.Log(":TODO:Don't destroy character to let it play its dead animation!");
        }
    }
}