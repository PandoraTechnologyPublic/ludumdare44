using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using static Pandora.Utils.GladiatorGenerator;

namespace Pandora.Components.MonoBehaviours
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private EnemyComponentProxy _enemyPrefab;
        private List<GladiatorCharacter> _enemyCharacters = new List<GladiatorCharacter>();
        private int _enemyToSpawnCount = 0;

        public void SpawnEnemy(string name, int maxLife, int damage)
        {
            _enemyToSpawnCount++;
            GladiatorCharacter enemyCharacter = new GladiatorCharacter();
            enemyCharacter._name = name;
            enemyCharacter._health = maxLife;
            enemyCharacter._strength = damage;

            _enemyCharacters.Add(enemyCharacter);
        }

        public void Reset()
        {
            _enemyCharacters.Clear();
            _enemyToSpawnCount = 0;
        }

        private void Update()
        {
            EntityManager activeEntityManager = World.Active.EntityManager;

            while (_enemyToSpawnCount > 0)
            {
                _enemyToSpawnCount--;
                GladiatorCharacter enemyCharacter = _enemyCharacters.ToArray()[_enemyToSpawnCount];

                EnemyComponentProxy enemyComponentProxy = GameObject.Instantiate(_enemyPrefab, transform.position, transform.rotation);
                Entity enemyEntity = enemyComponentProxy.GetComponent<EntityComponent>().GetEntity();

                // Set name
                EnemyNameLayout enemyNameLayout = enemyComponentProxy.GetComponent<EnemyNameLayout>();
                enemyNameLayout.SetNAme(enemyCharacter._name);
                
                // Set maxLife
                LifeComponent lifeComponent = activeEntityManager.GetComponentData<LifeComponent>(enemyEntity);
                lifeComponent._maxLife = enemyCharacter._health;
                lifeComponent._currentLife = lifeComponent._maxLife;
                activeEntityManager.SetComponentData(enemyEntity, lifeComponent);

                // Set damage
                AttackComponent attackComponent = activeEntityManager.GetComponentData<AttackComponent>(enemyEntity);
                attackComponent._damage = enemyCharacter._strength;
                activeEntityManager.SetComponentData(enemyEntity, attackComponent);
            }
        }
    }
}