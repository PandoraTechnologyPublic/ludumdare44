using Unity.Entities;

namespace Pandora
{
    public struct AttackComponent : IComponentData
    {
        public int _damage;
    }
}