using Unity.Entities;

namespace Pandora
{
    public struct PlayerComponent : IComponentData
    {
        public int _staminaCostPerDodge;
        public int _staminaCostPerAttack;
    }
}