using Unity.Entities;

namespace Pandora
{
    public struct GenerateLevelEventComponent : IComponentData
    {
        public bool _completed;
    }
}