using Unity.Entities;

namespace Pandora
{
    public struct LifeDecreaseEventComponent : IComponentData
    {
        public Entity _entity;
        public int _value;
    }
}