using Unity.Entities;

namespace Pandora
{
    public struct MoveComponent : IComponentData
    {
        public float _moveSpeed;
        public float _dodgeDuration;
        public float _dodgeSpeed;
    }
}