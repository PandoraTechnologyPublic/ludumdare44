using Unity.Entities;

namespace Pandora
{
    public struct StaminaComponent : IComponentData
    {
        public float _currentStamina;
        public int _maxStamina;
        public float _repletionPerSecond;
    }
}