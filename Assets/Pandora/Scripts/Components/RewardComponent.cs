using Unity.Entities;

namespace Pandora
{
    public struct RewardComponent : IComponentData
    {
        public float _rewardRatio;
        public int _currentReward;
    }
}