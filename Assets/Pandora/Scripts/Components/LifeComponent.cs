using Unity.Entities;

namespace Pandora
{
    public struct LifeComponent : IComponentData
    {
        public int _currentLife;
        public int _maxLife;
        public Entity _lastHitEntity;
    }
}