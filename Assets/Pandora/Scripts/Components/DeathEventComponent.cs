using Unity.Entities;

namespace Pandora
{
    public struct DeathEventComponent : IComponentData
    {
        public Entity _dyingEntity;
        public Entity _killerEntity;
    }
}