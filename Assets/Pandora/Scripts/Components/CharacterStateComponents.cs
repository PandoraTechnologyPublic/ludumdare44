using Unity.Entities;

namespace Pandora
{
    public struct IsIdleComponent : IComponentData { }

    public struct IsMovingComponent : IComponentData { }
    public struct IsDeadComponent : IComponentData { }

    public struct IsDodgingComponent : IComponentData
    {
    }

    public struct IsAttackingComponent : IComponentData
    {
        public Entity _attackEventEntity;
        public Entity _attackingEntity;
        public int _hitCount;
    }
}