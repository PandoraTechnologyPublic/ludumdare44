using Unity.Entities;

namespace Pandora
{
    public struct AttackMissEventComponent : IComponentData
    {
        public Entity _attackerEntity;
        public Entity _defenderEntity;
        public float _overlapDistance;
        public float _rewardTime;
    }
}