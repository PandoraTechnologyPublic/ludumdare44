using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    [RequiresEntityConversion]
    public class RewardComponentProxy : MonoBehaviour, IConvertGameObjectToEntity
    {
        public float _rewardRatio;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            RewardComponent data = new RewardComponent { _currentReward = 0, _rewardRatio = _rewardRatio };
            dstManager.AddComponentData(entity, data);
        }
    }
}