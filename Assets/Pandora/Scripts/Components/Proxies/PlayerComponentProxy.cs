using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    [RequiresEntityConversion]
    public class PlayerComponentProxy : MonoBehaviour, IConvertGameObjectToEntity
    {
        public int _staminaCostPerDodge;
        public int _staminaCostPerAttack;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            PlayerComponent data = new PlayerComponent { _staminaCostPerDodge = _staminaCostPerDodge, _staminaCostPerAttack = _staminaCostPerAttack};
            dstManager.AddComponentData(entity, data);
        }
    }
}