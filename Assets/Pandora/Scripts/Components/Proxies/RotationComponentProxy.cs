using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    [RequiresEntityConversion]
    public class RotationComponentProxy : MonoBehaviour, IConvertGameObjectToEntity
    {
        public float _rotationSpeed;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            RotationComponent data = new RotationComponent{_rotationSpeed = _rotationSpeed};
            dstManager.AddComponentData(entity, data);
        }
    }
}