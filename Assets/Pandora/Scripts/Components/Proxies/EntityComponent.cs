using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    public class EntityComponent : MonoBehaviour, IConvertGameObjectToEntity
    {
        private Entity _entity;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            _entity = entity;
        }

        public Entity GetEntity()
        {
            return _entity;
        }
    }
}