using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    [RequiresEntityConversion]
    public class MoveComponentProxy : MonoBehaviour, IConvertGameObjectToEntity
    {
        public float _moveSpeed;
        public float _dodgeDuration;
        public float _dodgeSpeed;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            MoveComponent data = new MoveComponent{_moveSpeed = _moveSpeed, _dodgeDuration = _dodgeDuration, _dodgeSpeed = _dodgeSpeed };
            dstManager.AddComponentData(entity, data);
        }
    }
}