using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    [RequiresEntityConversion]
    public class EnemyComponentProxy : MonoBehaviour, IConvertGameObjectToEntity
    {
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            EnemyComponent data = new EnemyComponent();
            dstManager.AddComponentData(entity, data);
        }
    }
}