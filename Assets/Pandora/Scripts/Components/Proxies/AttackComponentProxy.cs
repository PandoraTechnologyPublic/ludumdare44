using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    [RequiresEntityConversion]
    public class AttackComponentProxy : MonoBehaviour, IConvertGameObjectToEntity
    {
        public int _damage;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            AttackComponent data = new AttackComponent { _damage = _damage };
            dstManager.AddComponentData(entity, data);
        }
    }
}