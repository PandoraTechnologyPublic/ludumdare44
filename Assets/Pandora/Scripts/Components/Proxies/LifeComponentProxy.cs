using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    [RequiresEntityConversion]
    public class LifeComponentProxy : MonoBehaviour, IConvertGameObjectToEntity
    {
        public int _maxLife;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            LifeComponent data = new LifeComponent { _maxLife = _maxLife, _currentLife =  _maxLife};
            dstManager.AddComponentData(entity, data);
        }
    }
}