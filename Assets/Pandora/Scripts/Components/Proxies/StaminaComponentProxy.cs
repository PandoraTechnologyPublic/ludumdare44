using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    [RequiresEntityConversion]
    public class StaminaComponentProxy : MonoBehaviour, IConvertGameObjectToEntity
    {
        public int _maxStamina;
        public float _repletionPerSecond;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            StaminaComponent data = new StaminaComponent {_currentStamina = _maxStamina, _maxStamina = _maxStamina, _repletionPerSecond = _repletionPerSecond };
            dstManager.AddComponentData(entity, data);
        }
    }
}