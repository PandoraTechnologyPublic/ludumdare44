﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.UI
{
    public class HeroStatsImprover : MonoBehaviour
    {
        [SerializeField] public Slider _valueSlider;
        [SerializeField] public Image _valueSliderFill;
        [SerializeField] public TMP_Text _valueText;
        [SerializeField] public TMP_Text _labelText;
        [SerializeField] public Button _increaseButton;
        [SerializeField] public Button _decreaseButton;
    }
}
