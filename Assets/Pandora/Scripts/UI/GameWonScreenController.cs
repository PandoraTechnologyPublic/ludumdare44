﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.UI
{
    public class GameWonScreenController : MonoBehaviour
    {
        [SerializeField] private HeroStatsImprover _currentHealthBar;
        [SerializeField] private HeroStatsImprover _healthImprover;
        [SerializeField] private HeroStatsImprover _staminaImprover;
        [SerializeField] private HeroStatsImprover _attackImprover;
        [SerializeField] private Button _healButton;
        [SerializeField] private Button _finishButton;
        [SerializeField] private TMP_Text _pointsText;

        [SerializeField] private int _maxHealthPointBaseCost = 10;
        [SerializeField] private int _staminaPointBaseCost = 10;
        [SerializeField] private int _attackPointBaseCost = 100;
        
        private int _currentHealth;
        private int _currentAttack;
        private int _currentMaxHealth;
        private int _currentMaxStamina;
        private int _pointsToDistribute;
        private int _pointsDistributed;
        private int _increasedHealth;
        private int _increasedStamina;
        private int _increasedAttack;
        private int _newMaxHealth;

        public delegate void FinalizeDelegate(HeroImprovementsOrder order);
        FinalizeDelegate _finalizeDelegate;

        // Start is called before the first frame update
        private void Start()
        {
            _currentHealthBar._labelText.text = "Current Health";
            _currentHealthBar._valueSliderFill.color = Color.red;
            // Configure HEALTH improver
            _healthImprover._labelText.text = "Max Health:";
            _healthImprover._valueSliderFill.color = Color.red;
            _healthImprover._increaseButton.onClick.AddListener(OnIncreaseHealthButton);
            _healthImprover._decreaseButton.onClick.AddListener(OnDecreaseHealthButton);

            // Configure STAMINA improver
            _staminaImprover._labelText.text = "Stamina:";
            _staminaImprover._valueSliderFill.color = Color.yellow;
            _staminaImprover._increaseButton.onClick.AddListener(OnIncreaseStaminaButton);
            _staminaImprover._decreaseButton.onClick.AddListener(OnDecreaseStaminaButton);
            
            // Configure STAMINA improver
            _attackImprover._labelText.text = "Attack:";
            _attackImprover._valueSliderFill.color = Color.blue;
            _attackImprover._increaseButton.onClick.AddListener(OnIncreaseAttackButton);
            _attackImprover._decreaseButton.onClick.AddListener(OnDecreaseAttackButton);
            
            _healButton.onClick.AddListener(OnHealButton);
            
        }

        private void OnIncreaseHealthButton()
        {
            _pointsDistributed += GetNextMaxHealthPointCost(_currentMaxHealth + _increasedHealth, _maxHealthPointBaseCost);
            _increasedHealth++;
            
            RefreshUi();
        }
        private void OnDecreaseHealthButton()
        {
            _increasedHealth--;
            _pointsDistributed -= GetNextMaxHealthPointCost(_currentMaxHealth + _increasedHealth, _maxHealthPointBaseCost);
            
            RefreshUi();
        }

        private void OnIncreaseStaminaButton()
        {
            _pointsDistributed += GetNextStaminaPointCost(_currentMaxStamina + _increasedStamina, _staminaPointBaseCost);
            _increasedStamina++;
            
            RefreshUi();
        }
        private void OnDecreaseStaminaButton()
        {
            _increasedStamina--;
            _pointsDistributed -= GetNextStaminaPointCost(_currentMaxStamina + _increasedStamina, _staminaPointBaseCost);
            
            RefreshUi();
        }

        private void OnIncreaseAttackButton()
        {
            _pointsDistributed += GetNextAttackPointCost(_currentAttack + _increasedAttack, _attackPointBaseCost);
            _increasedAttack++;
            RefreshUi();
        }
        private void OnDecreaseAttackButton()
        {
            _increasedAttack--;
            _pointsDistributed -= GetNextAttackPointCost(_currentAttack + _increasedAttack, _attackPointBaseCost);
            
            RefreshUi();
        }

        private int GetNextAttackPointCost(int currentValue, int initialCost)
        {
            return initialCost * Mathf.FloorToInt(Mathf.Exp(currentValue + 1));
        }
        
        private int GetNextMaxHealthPointCost(int currentValue, int initialCost)
        {
            return Mathf.FloorToInt(Mathf.Pow(initialCost * currentValue, 1.5f));
        }
        
        private int GetNextStaminaPointCost(int currentValue, int initialCost)
        {
            return (initialCost * currentValue) - 60;
        }


        private void OnHealButton()
        {
            _pointsDistributed = _pointsToDistribute;
            _increasedHealth = 0;
            _increasedStamina = 0;
            _increasedAttack = 0;
            _newMaxHealth = _currentMaxHealth;
            _currentHealth = _currentMaxHealth;
            
            RefreshUi();
        }

        public void Initialize(
            int currentHealth, int currentMaxHealth, int currentMaxStamina, int currentAttack, int pointsToDistribute,
            FinalizeDelegate finalizeDelegate
            )
        {
            _currentHealth = currentHealth;
            _currentMaxHealth = currentMaxHealth;
            _currentMaxStamina = currentMaxStamina;
            _currentAttack = currentAttack;
            _pointsToDistribute = pointsToDistribute;
            _finalizeDelegate = finalizeDelegate;

            _increasedHealth = 0;
            _increasedStamina = 0;
            _increasedAttack = 0;
            _pointsDistributed = 0;
            
            RefreshUi();

        }

        private void OnDisable()
        {
            HeroImprovementsOrder order = new HeroImprovementsOrder();

            order._pointsDistributed = _pointsDistributed;
            order._newCurrentHealth = _currentHealth;
            order._increasedHealth = _increasedHealth;
            order._increasedStamina = _increasedStamina;
            order._increasedAttack = _increasedAttack;

            _finalizeDelegate(order);
        }

        private void RefreshUi()
        {
            RefreshButtons();
            RefreshSliders();
            _pointsText.text = "Remaining Money: " + (_pointsToDistribute - _pointsDistributed) + " / " + _pointsToDistribute;

        }

        private void RefreshSliders()
        {
            _currentHealthBar._valueSlider.value =  ((float)(_currentHealth) / (float)(_currentMaxHealth)) * 100;
            _currentHealthBar._valueText.text = $"{_currentHealth} / {_currentMaxHealth}";
            _healthImprover._valueSlider.value = ((float)(_currentMaxHealth + _increasedHealth) / (float)(_currentMaxHealth + _pointsToDistribute)) * 100;
            _healthImprover._valueText.text = $"{_currentMaxHealth} (Cost : {GetNextMaxHealthPointCost(_currentMaxHealth + _increasedHealth, _maxHealthPointBaseCost)})";
            if (_increasedHealth > 0)
            {
                _healthImprover._valueText.text += " + " + _increasedHealth;
            }

            _staminaImprover._valueSlider.value = ((float)(_currentMaxStamina + _increasedStamina) / (float)(_currentMaxStamina + _pointsToDistribute)) * 100;
            _staminaImprover._valueText.text = $"{_currentMaxStamina} (Cost : {GetNextStaminaPointCost(_currentMaxStamina + _increasedStamina, _staminaPointBaseCost)})";
            if (_increasedStamina > 0)
            {
                _staminaImprover._valueText.text += " + " + _increasedStamina;
            }

            _attackImprover._valueSlider.value = ((float)(_currentAttack + _increasedAttack) / (float)(_currentAttack + _pointsToDistribute)) * 100;
            _attackImprover._valueText.text = $"{_currentAttack} (Cost : {GetNextAttackPointCost(_currentAttack + _increasedAttack, _attackPointBaseCost)})";
            
            if (_increasedAttack > 0)
            {
                _attackImprover._valueText.text += " + " + _increasedAttack;
            }

        }

        private void RefreshButtons()
        {
            int pointsLeft = _pointsToDistribute - _pointsDistributed;

            _healButton.interactable = _currentHealth < _currentMaxHealth;
            
            _healthImprover._decreaseButton.interactable = _increasedHealth > 0;
            _healthImprover._increaseButton.interactable = GetNextMaxHealthPointCost(_currentMaxHealth + _increasedHealth, _maxHealthPointBaseCost) <= pointsLeft;
            
            _staminaImprover._decreaseButton.interactable = _increasedStamina > 0;
            _staminaImprover._increaseButton.interactable = GetNextStaminaPointCost(_currentMaxStamina + _increasedStamina, _staminaPointBaseCost) <= pointsLeft;

            _attackImprover._decreaseButton.interactable = _increasedAttack > 0;
            _attackImprover._increaseButton.interactable = GetNextAttackPointCost(_currentAttack + _increasedAttack, _attackPointBaseCost) <= pointsLeft;
        }
    }
    public class HeroImprovementsOrder
    {
        public int _increasedHealth;
        public int _newCurrentHealth;
        public int _increasedStamina;
        public int _pointsDistributed;
        public int _increasedAttack;
    }
}
