﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SoundBank", menuName = "Create sound bank")]
public class SoundBank : ScriptableObject
{
    public AudioClip[] sounds; 
}
