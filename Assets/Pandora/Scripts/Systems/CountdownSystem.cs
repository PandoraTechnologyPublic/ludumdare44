using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;

namespace Pandora.Systems
{
    public class CountdownSystem : JobComponentSystem
    {
        [BurstCompile]
        private struct CountdownJob : IJobForEach<CountdownComponent>
        {
            public float _deltaTime;

            public void Execute(ref CountdownComponent countdown)
            {
                if (countdown._timeRemaining > 0)
                {
                    countdown._timeRemaining -= _deltaTime;

                    if (countdown._timeRemaining < 0)
                    {
                        countdown._timeRemaining = 0;
                    }
                }
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            CountdownJob rotationJob = new CountdownJob
            {
                _deltaTime = Time.deltaTime
            };

            return rotationJob.Schedule(this, inputDeps);
        }
    }
}