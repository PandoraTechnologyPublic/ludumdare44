using Pandora.Components.MonoBehaviours;
using Pandora.Utils;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class AttackMissResolutionSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity eventEntity, ref AttackMissEventComponent attackMissComponent) =>
            {
                Entity attackerEntity = attackMissComponent._attackerEntity;
                
                if (attackerEntity != Entity.Null)
                {
                    Entity attackEventEntity = EntityManager.GetComponentData<IsAttackingComponent>(attackerEntity)._attackEventEntity;
                    AttackEventComponent attackEventComponent = EntityManager.GetComponentData<AttackEventComponent>(attackEventEntity);
                    
                    if (attackEventComponent._hitCount <= 0)
                    {
                        attackEventComponent._rewardTime = attackMissComponent._rewardTime;
                        attackEventComponent._lastDodgerEntity = attackMissComponent._defenderEntity;
                        PostUpdateCommands.SetComponent(attackEventEntity, attackEventComponent);
                        
                        if (EntityManager.HasComponent<RewardBehaviourComponent>(attackMissComponent._defenderEntity))
                        {
                            RewardBehaviourComponent rewardBehaviourComponent = EntityManager.GetComponentObject<RewardBehaviourComponent>(attackMissComponent._defenderEntity);

                            float currentMoneyReward = Utilities.ComputeReward(attackEventComponent._rewardTime);
                            rewardBehaviourComponent.SetRewardValue(currentMoneyReward);
                        }
                    }
                    else
                    {
                        if (EntityManager.HasComponent<RewardBehaviourComponent>(attackMissComponent._defenderEntity))
                        {
                            RewardBehaviourComponent rewardBehaviourComponent = EntityManager.GetComponentObject<RewardBehaviourComponent>(attackMissComponent._defenderEntity);

                            rewardBehaviourComponent.ShowRewardLostEffect();
                        }
                    }
                    
                    PostUpdateCommands.DestroyEntity(eventEntity);
                }
            });
        }
    }
}