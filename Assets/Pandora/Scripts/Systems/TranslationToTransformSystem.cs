using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    // This System is only required due to the fact that the SpriteRenderer is not implemented under full ECS project.
    // Due to this we need to user ConvertToEntity(*ConvertAndInjectMode*) to keep the Transform managed by "standard" Unity.
    // This system is responsible to apply any change from Translation (i.e. ECS mode) into change from Transform (i.e. legacy mode)
    public class TranslationToTransformSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref Translation translation) =>
            {
                Transform transform = EntityManager.GetComponentObject<Transform>(entity);
                
                transform.position = new Vector3(translation.Value.x, translation.Value.y, translation.Value.z);
            });
        }
    }
}