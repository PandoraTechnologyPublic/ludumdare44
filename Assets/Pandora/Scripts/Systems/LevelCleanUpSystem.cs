using Pandora.Components.MonoBehaviours;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class LevelCleanUpSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAll<CleanLevelEventComponent>().ForEach(eventEntity =>
            {
                Entities.WithAll<EnemyComponent>().ForEach(enemyEntity =>
                {
                    Transform enemy = EntityManager.GetComponentObject<Transform>(enemyEntity);

                    foreach (Collider componentsInChild in enemy.GetComponentsInChildren<Collider>())
                    {
                        componentsInChild.enabled = false;
                    }
                    
//                    GameObject.Destroy(enemy.gameObject);
                    PostUpdateCommands.DestroyEntity(enemyEntity);
                });
                
                Entities.WithAll<AttackHitEventComponent>().ForEach(attackHitEventEntity =>
                {
                    PostUpdateCommands.DestroyEntity(attackHitEventEntity);
                });
                
                PostUpdateCommands.DestroyEntity(eventEntity);
            });
        }
    }
}