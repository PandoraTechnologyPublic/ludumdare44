using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    public class StaminaRepletingSystem : JobComponentSystem
    {
        [BurstCompile]
        private struct RepleteJob : IJobForEach<StaminaComponent>
        {
            public float _deltaTime;

            public void Execute(ref StaminaComponent stamina)
            {
                if (stamina._currentStamina < stamina._maxStamina)
                {
                    stamina._currentStamina += _deltaTime * stamina._repletionPerSecond;

                    if (stamina._currentStamina > stamina._maxStamina)
                    {
                        stamina._currentStamina = stamina._maxStamina;
                    }
                }
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            RepleteJob job = new RepleteJob
            {
                _deltaTime = Time.deltaTime
            };

            return job.Schedule(this, inputDeps);
        }
    }
}