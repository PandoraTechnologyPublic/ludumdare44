using Pandora.Components.MonoBehaviours;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class AttackHitResolutionSystem : ComponentSystem
    {      
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity eventEntity, ref AttackHitEventComponent attackHitEventComponent) =>
            {
                Entity hitReceiverEntity = attackHitEventComponent._hitReceiverEntity;

                if ((hitReceiverEntity != Entity.Null) && EntityManager.Exists(hitReceiverEntity) && !EntityManager.HasComponent(hitReceiverEntity, typeof(IsDeadComponent)))
                {
                    LifeComponent lifeComponent = EntityManager.GetComponentData<LifeComponent>(hitReceiverEntity);
                    int lifeDecreaseValue = attackHitEventComponent._damage;
                    lifeComponent._currentLife -= lifeDecreaseValue;
                    lifeComponent._lastHitEntity = attackHitEventComponent._hitDealerEntity;

                    PostUpdateCommands.SetComponent(hitReceiverEntity, lifeComponent);

                    AnimationBehaviourComponent animationBehaviourComponent = EntityManager.GetComponentObject<AnimationBehaviourComponent>(hitReceiverEntity);
                    animationBehaviourComponent.StartHitAnimation();
//                    Debug.Log($"Deal {lifeDecreaseValue} - new life {lifeComponent._currentLife}");

                    Entity lifeDecreaseEvent = PostUpdateCommands.CreateEntity();
                    PostUpdateCommands.AddComponent(lifeDecreaseEvent, new LifeDecreaseEventComponent{_entity = hitReceiverEntity, _value = lifeDecreaseValue});

                    Entity hitDealerEntity = attackHitEventComponent._hitDealerEntity;
                    Entity attackEventEntity = EntityManager.GetComponentData<IsAttackingComponent>(hitDealerEntity)._attackEventEntity;
                    AttackEventComponent attackEventComponent = EntityManager.GetComponentData<AttackEventComponent>(attackEventEntity);

                    ++attackEventComponent._hitCount;

                    if (EntityManager.HasComponent<RewardBehaviourComponent>(hitReceiverEntity))
                    {
                        RewardBehaviourComponent rewardBehaviourComponent = EntityManager.GetComponentObject<RewardBehaviourComponent>(hitReceiverEntity);

                        rewardBehaviourComponent.ShowRewardLostEffect();
                    }        
                    
                    PostUpdateCommands.SetComponent(attackEventEntity, attackEventComponent);

                    PostUpdateCommands.DestroyEntity(eventEntity);
                }
            });
        }
    }
}