using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    public class DodgeCompleteSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {            
            Entities.ForEach((Entity eventEntity, ref DodgeEventComponent dodgeEventComponent , ref CountdownComponent countdownComponent) =>
            {
                if (countdownComponent._timeRemaining <= 0)
                {
                    PostUpdateCommands.DestroyEntity(eventEntity);
                    
                    Entity dodgingEntity = dodgeEventComponent._dodgingEntity;
                    PostUpdateCommands.RemoveComponent<IsDodgingComponent>(dodgingEntity);
                }
            });
        }
    }
}