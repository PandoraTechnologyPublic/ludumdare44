using System.ComponentModel;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;

namespace Pandora.Systems
{
    public class DeathCheckSystem : JobComponentSystem
    {
        private BeginInitializationEntityCommandBufferSystem _entityCommandBufferSystem;

        protected override void OnCreateManager()
        {
            base.OnCreateManager();
            
            _entityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
        }

        [ExcludeComponent(typeof(IsDeadComponent))]
        private struct DeathCheckJob : IJobForEachWithEntity<LifeComponent>
        {
            public EntityCommandBuffer _entityCommandBuffer;
            public float _deltaTime;

            public void Execute(Entity entity, int index, [ReadOnly(true)] ref LifeComponent lifeComponent)
            {
                if (lifeComponent._currentLife <= 0)
                {
                    _entityCommandBuffer.AddComponent(entity, new IsDeadComponent());
                    Entity dieEventEntity = _entityCommandBuffer.CreateEntity();
                    _entityCommandBuffer.AddComponent(dieEventEntity, new DeathEventComponent{_dyingEntity = entity, _killerEntity = lifeComponent._lastHitEntity});

//                    Debug.Log("Entity dies");
                }
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            DeathCheckJob deathCheckJob = new DeathCheckJob
            {
                _deltaTime = Time.deltaTime,
                _entityCommandBuffer = _entityCommandBufferSystem.CreateCommandBuffer()
            };

            JobHandle jobHandle = deathCheckJob.ScheduleSingle(this, inputDeps);

            _entityCommandBufferSystem.AddJobHandleForProducer(jobHandle);

            return jobHandle;
        }
    }
}