using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    // This System is only required due to the fact that the SpriteRenderer is not implemented under full ECS project.
    // Due to this we need to user ConvertToEntity(*ConvertAndInjectMode*) to keep the Transform managed by "standard" Unity.
    // This system is responsible to apply any change from Translation (i.e. ECS mode) into change from Transform (i.e. legacy mode)
    public class RotationToTransformSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref Rotation rotation) =>
            {
                Transform transform = EntityManager.GetComponentObject<Transform>(entity);
                
                transform.rotation = new Quaternion(rotation.Value.value.x, rotation.Value.value.y, rotation.Value.value.z, rotation.Value.value.w);
            });
        }
    }
}