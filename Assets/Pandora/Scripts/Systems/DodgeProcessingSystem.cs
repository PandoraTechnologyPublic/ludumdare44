using System.ComponentModel;
using Pandora.Utils;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora.Systems
{
    public class DodgeProcessingSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            float deltaTime = Time.deltaTime;
            
            Entities.WithAll<IsDodgingComponent>().ForEach((Entity entity, ref Translation translation, ref MoveComponent moveComponent, ref LocalToWorld localToWorld) =>
            {
                Vector3 moveDirection = math.transform(localToWorld.Value, Vector3.forward) - translation.Value;
                
                moveDirection.Normalize();
                
                float3 rayOrigin = translation.Value + new float3(moveDirection) + new float3(0,1,0);
            
//                    Debug.DrawRay(rayOrigin, moveDirection * _deltaTime * moveComponent._dodgeSpeed, Color.red, 0.1f);
            
                if (!Physics.Raycast(rayOrigin, moveDirection, deltaTime * moveComponent._dodgeSpeed))
                {
//                        float3 nextPosition = translation.Value + new float3(moveDirection) * Time.deltaTime * moveComponent._moveSpeed;
//                        translation.Value = nextPosition;
//                    translation.Value += new float3(moveDirection) * deltaTime * moveComponent._dodgeSpeed;
                    float3 nextPosition = translation.Value + new float3(moveDirection) * deltaTime * moveComponent._dodgeSpeed;
                    
                    if ((nextPosition.x < Utilities.ARENA_MAX_SIZE) && (nextPosition.x > -Utilities.ARENA_MAX_SIZE) && (nextPosition.z < Utilities.ARENA_MAX_SIZE) && (nextPosition.z > -Utilities.ARENA_MAX_SIZE))
                    {
                        translation.Value = nextPosition;
                    }
                }
            });
        }
    }
}