using Pandora.Components.MonoBehaviours;
using Unity.Entities;

namespace Pandora.Systems
{
    public class LifeDecreaseSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref LifeDecreaseEventComponent lifeDecreaseEventComponent) =>
            {
                LifeDecreaseLayout lifeDecreaseLayout = EntityManager.GetComponentObject<LifeDecreaseLayout>(lifeDecreaseEventComponent._entity);
                lifeDecreaseLayout.Initialize(lifeDecreaseEventComponent._value);
                lifeDecreaseLayout.StartLifeDecreaseAnimation();
                PostUpdateCommands.DestroyEntity(entity);
            });
        }
    }
}