using System;
using Pandora.Components.MonoBehaviours;
using Pandora.Utils;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    public class AttackCompleteSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity eventEntity, ref AttackEventComponent attackEventComponent, ref CountdownComponent countdownComponent) =>
            {
                if (countdownComponent._timeRemaining <= 0)
                {
                    if ((attackEventComponent._hitCount <= 0))// && (attackEventComponent._maxOverlapDistance > 0))
                    {
//                        Debug.Log("HAHAHAH I MISSED YOUR ATTACK (max miss collider overlap: " + attackEventComponent._maxOverlapDistance + ")");
//                        // TODO actual reward here, like money or smtg TO THE DEFENDER
                        Entity dodgerEntity = attackEventComponent._lastDodgerEntity;
                
                        if (EntityManager.HasComponent<RewardComponent>(dodgerEntity))
                        {
                            RewardComponent rewardComponent = EntityManager.GetComponentData<RewardComponent>(dodgerEntity);

                            int reward = Utilities.ComputeReward(attackEventComponent._rewardTime);
                            rewardComponent._currentReward += reward;
                    
                            EntityManager.SetComponentData(dodgerEntity, rewardComponent);

                            if (EntityManager.HasComponent<RewardBehaviourComponent>(dodgerEntity))
                            {
                                RewardBehaviourComponent rewardBehaviourComponent = EntityManager.GetComponentObject<RewardBehaviourComponent>(dodgerEntity);

                                rewardBehaviourComponent.SetRewardValue(reward);
                                rewardBehaviourComponent.ShowRewardWonEffect();
                            }
                        }
                    }

                    PostUpdateCommands.DestroyEntity(eventEntity);
                    
                    Entity attackingEntity = attackEventComponent._attackingEntity;
                    PostUpdateCommands.RemoveComponent<IsAttackingComponent>(attackingEntity);
                }
            });
        }
    }
}