using Pandora.Components.MonoBehaviours;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.Systems
{
    public class LevelGenerationSystem : ComponentSystem
    {
        // Simple dictionary with the Wave Index as the Key and WaveConfig as value
        //
        private Dictionary<int, WaveConfig> WAVE_CONFIGS = new Dictionary<int, WaveConfig>
        {
            {  1, new WaveConfig {_numberOfEnemies =  1,  _enemyMaxLife =  3, _enemyDamage = 1 } },
            {  2, new WaveConfig {_numberOfEnemies =  2,  _enemyMaxLife =  6, _enemyDamage = 2 } },
            {  3, new WaveConfig {_numberOfEnemies =  3,  _enemyMaxLife =  9, _enemyDamage = 3 } },
            {  4, new WaveConfig {_numberOfEnemies =  6,  _enemyMaxLife = 12, _enemyDamage = 4 } },
            {  5, new WaveConfig {_numberOfEnemies =  8,  _enemyMaxLife = 14, _enemyDamage = 5 } },
            {  6, new WaveConfig {_numberOfEnemies = 10,  _enemyMaxLife = 16, _enemyDamage = 6 } },
            {  7, new WaveConfig {_numberOfEnemies = 12,  _enemyMaxLife = 18, _enemyDamage = 7 } },
            {  8, new WaveConfig {_numberOfEnemies = 14,  _enemyMaxLife = 20, _enemyDamage = 8 } },
            {  9, new WaveConfig {_numberOfEnemies = 16,  _enemyMaxLife = 22, _enemyDamage = 9 } },
            { 10, new WaveConfig {_numberOfEnemies = 20,  _enemyMaxLife = 24, _enemyDamage = 10 } }
        };

        private struct WaveConfig
        {
            public int _numberOfEnemies;
            public int _enemyMaxLife;
            public int _enemyDamage;
        }

        protected override void OnUpdate()
        {
            GameStateManager gameStateManager = null;

            Entities
                .WithAll<GameStateManager>()
                .ForEach(entity =>
                {
                    Assert.IsNull(gameStateManager);
                    gameStateManager = EntityManager.GetComponentObject<GameStateManager>(entity);
                });

            int spawnedEnemyCount = 0;

            // Count already spawned enemies
            //
            Entities.WithAll<EnemyComponent>().ForEach(entity => { ++spawnedEnemyCount; });

            // If a GenerateLevelEvent is present then...
            //
            Entities.ForEach((Entity eventEntity, ref GenerateLevelEventComponent generateLevelEventComponent) =>
            {
                int currentWave = gameStateManager.GetCurrentWave();

                // Set enemy characteristics based on wave configuration
                //
                WaveConfig waveConfig;
                if (!WAVE_CONFIGS.TryGetValue(currentWave, out waveConfig))
                {
                    waveConfig = new WaveConfig { _numberOfEnemies = currentWave * 2, _enemyMaxLife = currentWave * 3, _enemyDamage = currentWave };
                }

                int enemySpawnCountLeft = waveConfig._numberOfEnemies;

                // Generate Enemies
                //
                string[] enemyNames = new string[enemySpawnCountLeft];

                // Dispatch enemies to spawners
                //
                List<EnemySpawner> spawners = new List<EnemySpawner>();
                Entities.WithAll<EnemySpawner>().ForEach(spawnerEntity => {
                    EnemySpawner spawner = EntityManager.GetComponentObject<EnemySpawner>(spawnerEntity);
                    spawner.Reset();
                    spawners.Add(spawner);
                 });

                while (enemySpawnCountLeft > 0)
                {
                    enemyNames[enemySpawnCountLeft-1] = gameStateManager.GenerateEnemy();
                    spawners.ToArray()[enemySpawnCountLeft % spawners.Count].SpawnEnemy(
                        enemyNames[enemySpawnCountLeft - 1],
                        waveConfig._enemyMaxLife,
                        waveConfig._enemyDamage);

                    enemySpawnCountLeft--;
                }

                // Remove the Event
                //
                PostUpdateCommands.DestroyEntity(eventEntity);
                gameStateManager.ChangeState<IngameState>();
            });
        }
    }
}