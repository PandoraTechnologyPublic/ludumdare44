using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.Systems
{
    public class HeroStatsSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            UiLayout uiLayout = null;

            Entities
                .WithAll<UiLayout>()
                .ForEach(entity =>
            {
                Assert.IsNull(uiLayout);
                uiLayout = EntityManager.GetComponentObject<UiLayout>(entity);
            });

            Entities
                .WithAll<PlayerComponent>()
                .ForEach((Entity entity, ref StaminaComponent staminaComponent, ref LifeComponent lifeComponent, ref RewardComponent rewardComponent) =>
                {
                    uiLayout.SetHeroHealthStats(lifeComponent._currentLife, lifeComponent._maxLife);
                    uiLayout.SetHeroStaminaStats(staminaComponent._currentStamina, staminaComponent._maxStamina);
                    uiLayout.SetHeroReward(rewardComponent._currentReward);
                });
        }
    }
}