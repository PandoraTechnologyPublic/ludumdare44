using Pandora.Components.MonoBehaviours;
using Unity.Entities;

namespace Pandora.Systems
{
    public class DodgeEventSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithNone<CountdownComponent>().ForEach((Entity eventEntity, ref DodgeEventComponent dodgeEventComponent) =>
            {
                Entity dodgingEntity = dodgeEventComponent._dodgingEntity;
                AnimationBehaviourComponent animationBehaviourComponent = EntityManager.GetComponentObject<AnimationBehaviourComponent>(dodgingEntity);
                animationBehaviourComponent.StartDodgeAnimation();
                PostUpdateCommands.AddComponent(eventEntity, new CountdownComponent{_timeRemaining = dodgeEventComponent._dodgeDuration});
            });
        }
    }
}