using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.Systems
{
    public class WaveStatsSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            GameStateManager gameStateManager = null;

            Entities
                .WithAll<GameStateManager>()
                .ForEach(entity =>
                {
                    Assert.IsNull(gameStateManager);
                    gameStateManager = EntityManager.GetComponentObject<GameStateManager>(entity);
                });

            UiLayout uiLayout = gameStateManager.GetUiLayout();

            int enemies = 0;
            Entities
                .WithAll<EnemyComponent>()
                .ForEach((Entity entity) => { enemies++; });

            int deadEnemies = 0;
            Entities
                .WithAll<EnemyComponent, IsDeadComponent>()
                .ForEach((Entity entity) => { deadEnemies++; });

            uiLayout.SetGameWaveInfo(gameStateManager.GetCurrentWave(), enemies - deadEnemies, deadEnemies);
        }
    }
}