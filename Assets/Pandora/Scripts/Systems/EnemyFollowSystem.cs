using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora.Systems
{
    public class EnemyFollowSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            float3 playerPosition = float3.zero;
            
            Entities.WithAll<PlayerComponent>().ForEach((ref Translation translation) =>
            {
                playerPosition = translation.Value;
            });
            
            Entities.WithAll<EnemyComponent>().WithNone<IsAttackingComponent, IsDeadComponent>().ForEach((Entity entity, ref MoveComponent moveComponent, ref Rotation rotation, ref Translation translation) =>
            {
                float3 targetDirection = playerPosition - translation.Value;
                Quaternion lookRotation = Quaternion.LookRotation(targetDirection);
                rotation.Value = lookRotation;
                
                float distance = Vector3.Distance(playerPosition, translation.Value);
                NavMeshAgent navMeshAgent = EntityManager.GetComponentObject<NavMeshAgent>(entity);
                navMeshAgent.speed = moveComponent._moveSpeed;
                navMeshAgent.SetDestination(playerPosition);
                translation.Value = new float3(navMeshAgent.nextPosition);
                navMeshAgent.isStopped = distance <= 2.0f;
            });
        }
    }
}