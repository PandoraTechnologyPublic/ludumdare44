using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    public class DodgeFromPlayerSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithNone<IsDodgingComponent, IsAttackingComponent>().ForEach((Entity playerEntity, ref MoveComponent moveComponent , ref InputComponent inputComponent, ref StaminaComponent staminaComponent, ref PlayerComponent playerComponent) =>
            {
                if (inputComponent._dodgeButtonStatus == 1)
                {
                    if (staminaComponent._currentStamina >= playerComponent._staminaCostPerDodge)
                    {
                        Entity dodgeEventEntity = PostUpdateCommands.CreateEntity();
                        PostUpdateCommands.AddComponent(dodgeEventEntity, new DodgeEventComponent {_dodgingEntity = playerEntity, _dodgeDuration = moveComponent._dodgeDuration});

                        staminaComponent._currentStamina -= playerComponent._staminaCostPerDodge;

                        PostUpdateCommands.AddComponent(playerEntity, new IsDodgingComponent( ));
                    }
                    else
                    {
                        Entities.WithAll<GameStateManager>().ForEach(entity =>
                        {
                            SoundManager soundManager = EntityManager.GetComponentObject<GameStateManager>(entity).GetComponentInChildren<SoundManager>();
                            
                            soundManager.PlayNotEnoughStamina();
                        });
                    }
                }
            });
        }
    }
}