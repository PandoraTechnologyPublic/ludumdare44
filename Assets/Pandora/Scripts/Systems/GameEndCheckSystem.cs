using System.Collections.Generic;
using Unity.Entities;
using UnityEngine.Assertions;

namespace Pandora.Systems
{
    public class GameEndCheckSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            GameStateManager gameStateManager = null;

            Entities
                .WithAll<GameStateManager>()
                .ForEach(entity =>
            {
                Assert.IsNull(gameStateManager);
                gameStateManager = EntityManager.GetComponentObject<GameStateManager>(entity);
            });

            if (gameStateManager.IsCurrentState<IngameState>())
            {
                int enemies = 0;
                Entities
                    .WithAll<EnemyComponent>()
                    .ForEach((Entity entity) => { enemies++; });

                int deadEnemies = 0;
                Entities
                    .WithAll<EnemyComponent, IsDeadComponent>()
                    .ForEach((Entity entity) => { deadEnemies++; });

                List<Entity> heroes = new List<Entity>();
                Entities
                    .WithAll<PlayerComponent>()
                    .ForEach((Entity entity) => { heroes.Add(entity); });

                int deadHeroes = 0;
                Entities
                    .WithAll<PlayerComponent, IsDeadComponent>()
                    .ForEach((Entity entity) => { deadHeroes++; });

                if (deadHeroes >= heroes.Count)
                {
                    gameStateManager.WaveLoose();
                }
                else if (deadEnemies >= enemies)
                {
                    Assert.AreEqual(1, heroes.Count);

                    Entity heroEntity = heroes[0];

                    int currentLife = EntityManager.GetComponentData<LifeComponent>(heroEntity)._currentLife;
                    int maxLife = EntityManager.GetComponentData<LifeComponent>(heroEntity)._maxLife;
                    int maxStamina = EntityManager.GetComponentData<StaminaComponent>(heroEntity)._maxStamina;
                    int currentReward = EntityManager.GetComponentData<RewardComponent>(heroEntity)._currentReward;
                    int currentAttack = EntityManager.GetComponentData<AttackComponent>(heroEntity)._damage;

                    gameStateManager.WaveWon(heroEntity, currentLife, maxLife, maxStamina, currentAttack, currentReward);
                }
            }
        }
    }
}