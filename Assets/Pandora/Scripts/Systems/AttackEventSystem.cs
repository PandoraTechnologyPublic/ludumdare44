using Unity.Entities;

namespace Pandora.Systems
{
    public class AttackEventSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithNone<CountdownComponent>().ForEach((Entity eventEntity, ref AttackEventComponent attackEventComponent) =>
            {
                Entity attackingEntity = attackEventComponent._attackingEntity;
                AttackBehaviourComponent attackBehaviourComponent = EntityManager.GetComponentObject<AttackBehaviourComponent>(attackingEntity);
                attackBehaviourComponent.StartAttackAnimation();
                PostUpdateCommands.AddComponent(eventEntity, new CountdownComponent{_timeRemaining = attackEventComponent._attackDuration});
            });
        }
    }
}