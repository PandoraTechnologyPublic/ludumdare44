using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    public class PlayerRotationSystem : JobComponentSystem
    {
        [ExcludeComponent(typeof(IsDeadComponent))]
        private struct RotationJob : IJobForEach<Rotation, InputComponent, RotationComponent>
        {
            public float deltaTime;

            public void Execute(ref Rotation rotation, ref InputComponent inputComponent, ref RotationComponent rotationComponent)
            {
                Quaternion quaternion = new Quaternion(rotation.Value.value.x,rotation.Value.value.y,rotation.Value.value.z,rotation.Value.value.w);
                Quaternion deltaQuaternion = Quaternion.AngleAxis(-inputComponent._rotationAxis * deltaTime * rotationComponent._rotationSpeed, Vector3.up);
                rotation.Value = quaternion * deltaQuaternion;//Quaternion.Lerp(quaternion, deltaQuaternion, 1);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            RotationJob rotationJob = new RotationJob
            {
                deltaTime = Time.deltaTime
            };

            return rotationJob.Schedule(this, inputDeps);
        }
    }
}