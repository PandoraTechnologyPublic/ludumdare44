using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

namespace Pandora
{
    public class InputSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAll<GameStateManager>().ForEach(gameStateManagerEntity =>
            {
                GameStateManager gameStateManager = EntityManager.GetComponentObject<GameStateManager>(gameStateManagerEntity);
                
                Entities.WithNone<IsDeadComponent>().ForEach((ref InputComponent inputComponent) =>
                {
                    float horizontalAxis = 0;
                    float verticalAxis = 0;
                    float rotationAxis = 0;
                    bool isInteractButtonPressed = false;
                    bool isDodgeButtonPressed = false;
                    
                    if (gameStateManager.IsCurrentState<IngameState>())
                    {
                        horizontalAxis = Input.GetAxis("Horizontal");
                        verticalAxis = Input.GetAxis("Vertical");
                        rotationAxis = Input.GetAxis("Rotate");
                        isInteractButtonPressed = Input.GetButtonDown("Fire1");
                        isDodgeButtonPressed = Input.GetButtonDown("Dodge");
                    }

                    inputComponent._horizontalAxis = horizontalAxis;
                    inputComponent._verticalAxis = verticalAxis;
                    inputComponent._rotationAxis = rotationAxis;
                    inputComponent._attackButtonStatus = isInteractButtonPressed ? 1 : 0;
                    inputComponent._dodgeButtonStatus = isDodgeButtonPressed ? 1 : 0;
                });
            });
        }
    }
}