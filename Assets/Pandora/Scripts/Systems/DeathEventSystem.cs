using Pandora.Components.MonoBehaviours;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora.Systems
{
    public class DeathEventSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity eventEntity, ref DeathEventComponent deathEventComponent) =>
            {
                Entity killerEntity = deathEventComponent._killerEntity;
                
                if (EntityManager.HasComponent<RewardComponent>(killerEntity))
                {
                    RewardComponent rewardComponent = EntityManager.GetComponentData<RewardComponent>(killerEntity);

                    rewardComponent._currentReward += 1;
                    
                    EntityManager.SetComponentData(killerEntity, rewardComponent);
                }

                Entity dyingEntity = deathEventComponent._dyingEntity;
                
                if (EntityManager.HasComponent<NavMeshAgent>(dyingEntity))
                {
                    NavMeshAgent navMeshAgent = EntityManager.GetComponentObject<NavMeshAgent>(dyingEntity);
                    navMeshAgent.enabled = false;
                }
                
                // here the enemy is dead
                Transform transform = EntityManager.GetComponentObject<Transform>(dyingEntity);
                // remove its collider
                foreach (Rigidbody rigidbody in transform.GetComponentsInChildren<Rigidbody>())
                {
                    GameObject.Destroy(rigidbody);
                    
                }
                
                if (EntityManager.HasComponent<EnemyNameLayout>(dyingEntity))
                {
                    EnemyNameLayout enemyNameLayout = EntityManager.GetComponentObject<EnemyNameLayout>(dyingEntity);
                    enemyNameLayout.DisableLayout();
                }

                AnimationBehaviourComponent animationBehaviourComponent = EntityManager.GetComponentObject<AnimationBehaviourComponent>(dyingEntity);
                animationBehaviourComponent.StartDeathAnimation();
                
//                PostUpdateCommands.AddComponent(eventEntity, new CountdownComponent{_timeRemaining = attackEventComponent._attackDuration});
                PostUpdateCommands.DestroyEntity(eventEntity);
            });
        }
    }
}