using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    public class AttackFromPlayerSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithNone<IsAttackingComponent, IsDeadComponent>().ForEach((Entity playerEntity, ref Translation translation, ref InputComponent inputComponent, ref StaminaComponent staminaComponent, ref PlayerComponent playerComponent) =>
            {
                if (inputComponent._attackButtonStatus > 0)
                {
                    if (staminaComponent._currentStamina >= playerComponent._staminaCostPerAttack)
                    {
                        Entity attackEventEntity = PostUpdateCommands.CreateEntity();
                        PostUpdateCommands.AddComponent(attackEventEntity, new AttackEventComponent { _attackingEntity = playerEntity, _attackDuration = 0.6f });
                        PostUpdateCommands.AddComponent(playerEntity, new IsAttackingComponent {_attackEventEntity = attackEventEntity});
                    
                        staminaComponent._currentStamina -= playerComponent._staminaCostPerAttack;
                    }
                    else
                    {
                        Entities.WithAll<GameStateManager>().ForEach(entity =>
                        {
                            SoundManager soundManager = EntityManager.GetComponentObject<GameStateManager>(entity).GetComponentInChildren<SoundManager>();
                            
                            soundManager.PlayNotEnoughStamina();
                        });
                    }
                }
            });
        }
    }
}