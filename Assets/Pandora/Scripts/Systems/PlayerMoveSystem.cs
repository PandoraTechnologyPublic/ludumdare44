using Pandora.Utils;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora.Systems
{
    public class PlayerMoveSystem : ComponentSystem
    {
        private Camera cam;

        protected override void OnCreate()
        {
            cam = Camera.main;
        }

        protected override void OnUpdate()
        {
            if (!cam)
            {
                cam = Camera.main;
            }
            
            Entities
                .WithAll<PlayerComponent>()
                .WithNone<IsMovingComponent, IsDodgingComponent, IsAttackingComponent>()
                .ForEach((Entity entity, ref InputComponent inputComponent) =>
            {
                if (inputComponent._horizontalAxis != 0 || inputComponent._verticalAxis != 0)
                {
                    PostUpdateCommands.AddComponent(entity, new IsMovingComponent());
                }
            });

            Entities
                .WithAll<PlayerComponent, IsMovingComponent>()
                .WithNone<IsDodgingComponent/*, IsAttackingComponent*/>()
                .ForEach((Entity entity, ref Translation translation, ref Rotation rotation, ref InputComponent inputComponent, ref MoveComponent moveComponent, ref LocalToWorld localToWorld) =>
            {
                Vector3 camFwd = new Vector3(cam.transform.forward.x, 0, cam.transform.forward.z).normalized;
                Vector3 camRight = new Vector3(cam.transform.right.x, 0, cam.transform.right.z);

                Vector3 moveDirection = camFwd * inputComponent._verticalAxis + camRight * inputComponent._horizontalAxis;
                
                if (moveDirection.sqrMagnitude > 0)
                {
                    Quaternion rot = Quaternion.LookRotation(moveDirection);
                    rotation.Value.value = new float4(rot.x, rot.y, rot.z, rot.w);
                }
                
                if (moveDirection.sqrMagnitude > 1.0f)
                {
                    moveDirection = moveDirection.normalized;
                }

                float3 rayOrigin = translation.Value + new float3(moveDirection.normalized) + new float3(0,1,0);
                
                Debug.DrawRay(rayOrigin, moveDirection.normalized, Color.red, 0.1f);
                
                if (!Physics.Raycast(rayOrigin, moveDirection.normalized, 1.0f))
                {
                    float3 nextPosition = translation.Value + new float3(moveDirection) * Time.deltaTime * moveComponent._moveSpeed;
                    
                    if ((nextPosition.x < Utilities.ARENA_MAX_SIZE) && (nextPosition.x > -Utilities.ARENA_MAX_SIZE) && (nextPosition.z < Utilities.ARENA_MAX_SIZE) && (nextPosition.z > -Utilities.ARENA_MAX_SIZE))
                    {
                        translation.Value = nextPosition;
                    }
                }
            });
        }
    }
}