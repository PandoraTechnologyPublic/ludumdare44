using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Pandora.Systems
{
    public class AttackFromEnemySystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            float3 playerPosition = float3.zero;
            
            Entities.WithAll<PlayerComponent>().ForEach((ref Translation translation) =>
            {
                playerPosition = translation.Value;
            });
            
            Entities.WithAll<EnemyComponent>().WithNone<IsAttackingComponent, IsDeadComponent>().ForEach((Entity enemyEntity, ref Translation translation, ref Rotation rotation, ref LocalToWorld localToWorld) =>
            {
                float3 playerDirection = playerPosition - translation.Value;
                float3 forward = localToWorld.Forward;
                Vector3 normalizedPlayerDirection = Vector3.Normalize(playerDirection);
                Vector3 normalizedForwardAxis = Vector3.Normalize(forward);
//                Debug.Log(normalizedPlayerDirection);
//                Debug.Log(normalizedForwardAxis);
//                Debug.DrawRay(translation.Value, normalizedPlayerDirection, Color.red, 0.2f);
//                Debug.DrawRay(translation.Value, normalizedForwardAxis, Color.blue, 0.2f);

                float angle = Vector3.Angle(normalizedPlayerDirection, normalizedForwardAxis);
//                Debug.Log(angle);
                float distance = Vector3.Distance(playerPosition, translation.Value);
                
                if ((distance < 2.0f) && (angle < 25.0f))
                {
                    Entity attackEventEntity = PostUpdateCommands.CreateEntity();
                    PostUpdateCommands.AddComponent(attackEventEntity, new AttackEventComponent {_attackingEntity = enemyEntity, _attackDuration = 2.0f});
                    PostUpdateCommands.AddComponent(enemyEntity, new IsAttackingComponent{_attackEventEntity = attackEventEntity});
                }
            });
        }
    }
}